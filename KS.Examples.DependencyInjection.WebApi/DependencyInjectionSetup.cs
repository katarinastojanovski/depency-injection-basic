﻿using KS.Examples.DependencyInjection.Business;
using KS.Examples.DependencyInjection.Data;
using KS.Examples.DependencyInjection.Data.InMemory;
using Microsoft.Extensions.DependencyInjection;

namespace KS.Examples.DependencyInjection.WebApi
{
    public static class DependencyInjectionSetup
    {
        public static void AddMyServices(this IServiceCollection services)
        {
            // All in one place.
            // This adds unnecessary project dependency to KS.Examples.DependencyInjection.Data.InMemory.
            services.AddSingleton<IRandomizer, Randomizer>();
            services.AddSingleton<IArticlesRepository, InMemoryArticlesRepository>();
            services.AddSingleton<ITitleChecker, TitleChecker>();
            services.AddSingleton<IArticlesService, ArticlesService>();
        }
    }
}
