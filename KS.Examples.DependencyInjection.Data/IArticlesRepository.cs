﻿using KS.Examples.DependencyInjection.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KS.Examples.DependencyInjection.Data
{
    public interface IArticlesRepository
    {
        Task<IEnumerable<Article>> GetAll();

        Task<Article> GetById(Guid id);

        Task<Article> Add(Article article);
    }
}